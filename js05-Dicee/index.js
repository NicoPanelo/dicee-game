//set random numbers variable
let randonNumber1 = Math.random();
randonNumber1 = Math.floor(randonNumber1 * 6) + 1;

let randonNumber2 = Math.random();
randonNumber2 = Math.floor(randonNumber2 * 6) + 1;

//array of dice images
let diceImages = ["./images/dice1.png", "./images/dice2.png", "./images/dice3.png", "./images/dice4.png", "./images/dice5.png", "./images/dice6.png"];

//get element using querySelector
const leftDice = document.querySelector(".img1");
const rightDice = document.querySelector(".img2");

//set the src attribute
leftDice.setAttribute("src", diceImages[randonNumber1 - 1]);
rightDice.setAttribute("src", diceImages[randonNumber2 - 1]);

//determination of winner
let result = document.querySelector("h1");

if (randonNumber1 > randonNumber2) {
	result.innerHTML = "Player 1 Wins!";
} else if (randonNumber2 > randonNumber1) {
	result.innerHTML = "Player 2 Wins!";
} else {
	result.innerHTML = "Draw";
} 





